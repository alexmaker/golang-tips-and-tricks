package errorwrapping

import (
	"database/sql"
	"errors"
	"fmt"
)

func bar2() error {
	if err := foo(); err != nil {
		return fmt.Errorf("bar failed: %w", foo())
	}
	return nil
}

func foo2() error {
	return fmt.Errorf("foo failed: %w", sql.ErrNoRows)
}

func main2() {
	err := bar()
	if errors.Is(err, sql.ErrNoRows) {
		fmt.Printf("data not found,  %+v\n", err)
		return
	}
	if err != nil {
		// unknown error
	}
}
