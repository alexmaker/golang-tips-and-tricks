package factory_method

type Conn struct {
}

type HandlerFactory func() Handler

type Server struct {
	commands  map[string]HandlerFactory
}

type Handler interface {
	Handle(conn Conn) error
}

type Noop struct {
}

func (n *Noop) Handle(conn Conn) error {
	return nil
}

type Logout struct {
}

func (n *Logout) Handle(conn Conn) error {
	return nil
}

func NewServer() *Server {
	s := new(Server)
	s.commands = map[string]HandlerFactory{
		"NOOP":       func() Handler { return &Noop{} },
		"LOGOUT":     func() Handler { return &Logout{} },
	}

	return s
}